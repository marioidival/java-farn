import javax.swing.JOptionPane;
/*
 * Faca um programa q leia n valores reais. Armazene estes valores num vetor. 
 * 
 * 
 * Ao final, imprima a média aritmética destes valores.  .
 */

/**
 *
 * @author mario
 */
public class ZeroTres {
   public static void main(String[] args){
       
       int termos, cont=0;
       double media, soma =0;
       
       termos = Integer.parseInt(JOptionPane.showInputDialog("Insira a quantidade :"));
       
       double valores[] = new double[termos];
       
       for(int i = 0; i< valores.length; i++){
           valores[i] = Double.parseDouble(JOptionPane.showInputDialog("Insira o numero"));
           
           soma += valores[i];
           cont++;
       }
       
       media = soma / cont;
       JOptionPane.showMessageDialog(null, media);
       
   } 
}
