import javax.swing.JOptionPane;

public class Data {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int diaUm, diaDois, mesUm, mesDois, anoUm, anoDois;
		String dataUm, dataDois;
		
		diaUm = Integer.parseInt(JOptionPane.showInputDialog("Informe o Dia:"));
		mesUm = Integer.parseInt(JOptionPane.showInputDialog("Informe o mes :"));
		anoUm = Integer.parseInt(JOptionPane.showInputDialog("Informe o ano:"));
		
		diaDois = Integer.parseInt(JOptionPane.showInputDialog("Informe o Dia 2:"));
		mesDois = Integer.parseInt(JOptionPane.showInputDialog("Informe o mes 2:"));
		anoDois = Integer.parseInt(JOptionPane.showInputDialog("Informe o ano 2:"));
		
		dataUm = diaUm +"/"+mesUm+"/"+anoUm;
		dataDois = diaDois+"/"+mesDois+"/"+anoDois;
	
		
		if(anoUm > anoDois){
			JOptionPane.showMessageDialog(null, "Data 1 Maior "+ dataUm);
			JOptionPane.showMessageDialog(null, "Data 2 Menor "+ dataDois);
		}else if(anoUm == anoDois){
			if (mesUm > mesDois){
				JOptionPane.showMessageDialog(null, "Data 1 Maior "+ dataUm);
				JOptionPane.showMessageDialog(null, "Data 2 Menor "+ dataDois);
			}else if(mesUm == mesDois){
				if(diaUm > diaDois){
					JOptionPane.showMessageDialog(null, "Data 1 Maior "+ dataUm);
					JOptionPane.showMessageDialog(null, "Data 2 Menor "+ dataDois);
				}else if(diaUm == diaDois){
					JOptionPane.showMessageDialog(null, "Datas Iguais, Repita novamente");
				}else{
					JOptionPane.showMessageDialog(null, "Data 2 Maior "+ dataDois);
					JOptionPane.showMessageDialog(null, "Data 1 Menor "+ dataUm);
				}
			}else{
				JOptionPane.showMessageDialog(null, "Data 2 Maior "+ dataDois);
				JOptionPane.showMessageDialog(null, "Data 1 Menor "+ dataUm);
			}
		}else{
			JOptionPane.showMessageDialog(null, "Data 2 Maior "+ dataDois);
			JOptionPane.showMessageDialog(null, "Data 1 Menor "+ dataUm);
		}
	}

}
