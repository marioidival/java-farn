/*
 * Um supermercado 24h deseja realizar uma pesquisa sobre a venda de produtos em
 * horários diferentes e o tipo de clientela de cada um deles. 
 * Para isso, o supermercado vai analisar 5  produtos específicos, 
 * cada um deles com códigos de produto e preços diferentes 
 * (Prod 1 –R$5,00; Prod 2 – R$15,00; Prod 3 – R$3,00; Prod 4 – R$10,00; 
 * Prod 5 – R$1,00, respectivamente);
 * a quantidade de cada um dos produtos comprados por cada cliente; 
 * o horário de venda 
 * ( horário 1: 06h às 12h; horário 2: 12h às 18h; horário 3: 18h às 24h; horário 4: 0h às 6h). 
 * Não se sabe quantos produtos nem quantos clientes são atendidos no supermercado, 
 * então crie um programa que pergunte quando o vendedor deseja parar de contabilizar
 * produtos de um cliente, e quando o vendedor não deseja mais atender nenhum cliente. 
 * Ao final do cadastro das vendas mostre as seguintes estatísticas:
 * 
 * a)Quantidade total de clientes atendidos - ok
 * b)Quantidade total de produtos vendidos no Horário 1 - ok
 * c)Horário com menor quantidade de produtos vendidos e o valor total destas vendas neste horário 
 * d)Compra mais cara registrada -> vetor
 * e)Quantidade de clientes que compraram apenas o produto 1 e o produto 2.
 */
import javax.swing.JOptionPane;

/**
 *
 * @author mario
 */
public class Mercado {
    
    public static void main(String[] args){

        int contClient=0, contProd, contPri=0, contSeg=0, contTer=0, contQua=0;
        int horario,fimProd;
        double somaProd=0.0, finalProdUm = 0,finalProdDois = 0, finalProdTres = 0, finalProdQua = 0 , totalCompra=0, auxCompra=0;
        String acao, acaoCaixa;
        
        
        // vetores
        double quantidade[] = new double[200];
        int compra[] = new int[10];
        
        for(int i=1; i > 0; i++){
            horario = Integer.parseInt(JOptionPane.showInputDialog("Informe o Turno: "));
            for(int j = 1; i > 0; j++){
                              
                do{
                    fimProd = Integer.parseInt(JOptionPane.showInputDialog("Insira o produto\n0 para Sair"));
                    
                    switch(fimProd){
                        case 1:
                            somaProd += 5.00;
                            break;
                        case 2:
                            somaProd += 15.00;
                            break;
                        case 3:
                            somaProd += 3.00;
                            break;
                        case 4:
                            somaProd += 10.00;
                            break;
                        case 5:
                            somaProd += 1.00;
                            break;
                        case 0:
                            JOptionPane.showMessageDialog(null,"Saindo");
                            break;
                        default:
                            JOptionPane.showMessageDialog(null,"Insira um produto valido");
                    }
                    
                    if(horario == 1 ){
                        finalProdUm = somaProd;
                        contPri++;
                        
                    }else if(horario == 2){
                        finalProdDois = somaProd;
                        contSeg++;
                        
                    }else if(horario == 3){
                        finalProdTres = somaProd;
                        contTer++;
                        
                    }else if(horario == 4){
                        finalProdQua = somaProd;
                        contQua++;
                    }else{
                        JOptionPane.showMessageDialog(null,"Insira um horario correto");
                    }
                        
                }while(fimProd != 0);
                        
                
                acao = JOptionPane.showInputDialog("Deseja Finalizar a Compra ?");
                if(acao.equals("sim")){
                    // adicionar vetor para pegar todas as compras
                    totalCompra = finalProdUm + finalProdDois + finalProdTres + finalProdQua;
                    
                    quantidade[j] = totalCompra;
                    
                    contClient++;
                    
                    break;
                }else{
                    continue;
                }
            }
            // contabilizar os dados do consumidor.
            
            acaoCaixa = JOptionPane.showInputDialog("Deseja Fechar o Caixa ?");
            if(acaoCaixa.equals("sim")){
                JOptionPane.showMessageDialog(null,"Quantidade de Clientes atendidos: "+ contClient);
                JOptionPane.showMessageDialog(null,"Quantidade de produtos vendidos no horario 1: "+ contPri);
                
                for(int x = 0; x < 4; x++){
                    
                    
                }
                
                
                
                break;
                
            }else{
                continue;
            }
            //mostrar as estatistica pedida
            
        }
    }
}
