import javax.swing.JOptionPane;

/**
 *  Criar um vetor com os n primeiros termos da sequência de Fibonacci
 * @author mario
 */
public class ExemploDois {
    
    public static void main(String[] args){
        
        int nTermos, cont=0;
        nTermos = Integer.parseInt(JOptionPane.showInputDialog("Insira a quantidade de termos da serie:"));
        
        int fibo[] = new int[nTermos];
        
        while(cont < nTermos){
            fibo[0] = 1;
            fibo [1] = 1;
            
            for(int i = 2; i < nTermos; i++){
                fibo[i]= fibo[i - 2] + fibo[i - 1];
            }
            
            JOptionPane.showMessageDialog(null,"Sequencia Fibbonacci");
            for(int j =0; j < nTermos; j++){
                JOptionPane.showMessageDialog(null,fibo[j]);
                
            }
            cont++;
        }
    }
}
