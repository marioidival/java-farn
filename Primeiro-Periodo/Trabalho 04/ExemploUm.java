import javax.swing.JOptionPane;
/**
 *  Imprimir a soma de N números dados pelo usuário
 * @author mario
 */
public class ExemploUm {
    
    public static void main(String[] args){
        
        int numeros;        
        numeros = Integer.parseInt(JOptionPane.showInputDialog("Quantos numeros para somar: "));
        double soma[] = new double[numeros];
        double aux = 0;
        for(int i = 0; i < numeros; i++){
            soma[i] = Double.parseDouble(JOptionPane.showInputDialog("Insira o numero"));
            
        }
        for(int j =0; j < numeros; j++){
            aux += soma[j];
        }
        
        JOptionPane.showMessageDialog(null,"A soma foi:" + aux);
        
    }
}
