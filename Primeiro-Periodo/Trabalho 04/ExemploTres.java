import javax.swing.JOptionPane;
/**
 *  Dados n números inteiros positivos, imprimi-los, separadamente, como pares e ímpares.
 * @author mario
 */
public class ExemploTres {
    
    public static void main(String[] args){
        
        int n,k,p,i;
        
        do{
            n = Integer.parseInt(JOptionPane.showInputDialog("Insira a quantidade"));
        }while(n == 0);
        
        int x[] = new int[n];
        int par[] = new int[n];
        int impar[] = new int[n];
        p =0;
        i =0;
        
        JOptionPane.showMessageDialog(null,"Insira os "+n+" numeros positivos");
        
        for(k=0; k < n; k++){
            do{
                x[k] = Integer.parseInt(JOptionPane.showInputDialog(" :"));
            }while(x[k] < n);
            
            if(x[k]/2*2 == x[k]){
                p += 1;
                par[p] = x[k];
            }else{
                i += 1;
                impar[i] = x[k];
            }
            
        }
        JOptionPane.showMessageDialog(null,"Numeros Peres");
        for(k=0;k < p; k++){
            JOptionPane.showMessageDialog(null, par[k]);
        }
          JOptionPane.showMessageDialog(null,"Numeros Impar");
        for(k=0;k < i; k++){
            JOptionPane.showMessageDialog(null, impar[k]);
        }
        
        
    }
    
}
