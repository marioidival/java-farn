import javax.swing.JOptionPane;


class Supermercado{

	/*
	Um supermercado 24h deseja realizar uma pesquisa sobre a venda de produtos em horários
	diferentes e o tipo de clientela de cada um deles. Para isso, o supermercado vai analisar 5
	produtos específicos, cada um deles com códigos de produto e preços diferentes (Prod 1 –
	R$5,00; Prod 2 – R$15,00; Prod 3 – R$3,00; Prod 4 – R$10,00; Prod 5 – R$1,00,
	respectivamente); a quantidade de cada um dos produtos comprados por cada cliente; o
	horário de venda (horário 1: 06h às 12h; horário 2: 12h às 18h; horário 3: 18h às 24h;
	horário 4: 0h às 6h). Não se sabe quantos produtos nem quantos clientes são atendidos no
	supermercado, então crie um programa que pergunte quando o vendedor deseja parar de
	contabilizar produtos de um cliente, e quando o vendedor não deseja mais atender nenhum
	cliente. Ao final do cadastro das vendas mostre as seguintes estatísticas:
		a)Quantidade total de clientes atendidos
		b)Quantidade total de produtos vendidos no Horário 1
		c)Horário com menor quantidade de produtos vendidos e o valor total destas vendas neste horário
		d)Compra mais cara registrada
		e)Quantidade de clientes que compraram apenas o produto 1 e o produto 2.

	*/

	public static void main(String[] args){
		

	}
}