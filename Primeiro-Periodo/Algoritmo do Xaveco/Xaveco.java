import javax.swing.JOptionPane;

/*
 * Algoritmo do Xaveco
 * autores : Mário Idival & Edmar Diorgenes
 */

class Xaveco{

  public static int pontos(String chaveco, int et){

    int pontuacao = 0;
    String chaves[] = null;
    String[] xav = chaveco.split(" ");

    if(et == 1){
        chaves = new String[] { "beijo", "quero" , "envolvente" , "charmosa" , "suave" , "fascinante" , "supreendente" , "poderosa" , "linda" , "porfavor" };
    }else if(et == 2){
        chaves = new String[] { "desculpa" , "tudobom?" , "sincero" , "simpatica" , "inteligente" , "divertida" , "educada" , "obrigado" , "tudobem" , "prazer" };
    }else if(et == 3){
        chaves = new String[] { "bom" , "conhecer" , "olhar" , "sorriso" , "outro lugar" , "melhor" , "telefone" , "cativante" , "festa" , "dança"};
    }else if(et == 4){
        chaves = new String[] { "coladinho" , "acordar" , "comigo" , "posso" , "legal" , "carinhosa" , "cheirosa" , "alegre" , "pernas bonitas" , "dedicada" };
    }else {
        chaves = new String[] { "gostei" , "cabelo" , "maravilhosa" , "especial" , "diferente" , "sexy" , "fofa" , "meiga" , "atraente" , "elegante" };
    }

    // Verifica em uma busca simples se existe no xaveco as palavras chaves
    for(int i = 0; i < chaves.length; i++){
      for(int j = 0; j < xav.length ; j++){
        if(xav[j].equals(chaves[i])) {
            pontuacao++;
        }
      }
    }
   // retorna a quantidade de pontos na rodada
    return pontuacao;
  }


  public static void senario(){
    String nome;
    nome = JOptionPane.showInputDialog("Insira o nome do jogador:");
    JOptionPane.showMessageDialog(null, nome + " está saindo do trabalho e resolve passar em um Pub para se "+
        "divertir um pouco depois de uma rotina pesada.\nAo chegar no Pub,"+ nome+" encontra uma linda mulher.Mas "+ nome +" se depara com um problema. Ele é timido e não sabe muito bem como abordar uma mulher.\nEntão "+ nome +" resolve criar alguns xavecos/cantadas e resolve arriscar conquistar aquela linda mulher que ele acaba de encontrar.     ");
  }


  public static void main(String args[]){
    senario();

    String likes[] = {"","Risos","Olhar Brilhante","Pegada na mão","Risos mais proximos","Beijos"};
    String foras[] = {"","Melhor você ir curtir sua festinha...","Vou ali no Banheiro e volto nunca mais","não não, melhor você ir dar uma voltinha","Sai daqui cara chato","Melhor você dar essas cantadinhas para uma garotinha"};
    int etapa = 1;
    String xaveco;
    int pont;

    for(int x = 1; x < 4 ; x++){
      
      xaveco = JOptionPane.showInputDialog("Etapa " + etapa +"\nInsira o Xaveco: ");
      pont = pontos(xaveco, etapa);

      if(pont >= 3){
        etapa++;
        x = 1;
        JOptionPane.showMessageDialog(null,likes[etapa]);
      }else if(pont >= 5 && etapa == 4){
        etapa++;
        x = 1;
        JOptionPane.showMessageDialog(null,likes[etapa]);
      }else if(pont >= 5 && etapa == 5){
        JOptionPane.showMessageDialog(null,"E Finalmente, Ela atende a sede do xaveco!");

      }else if(x == 3){
        JOptionPane.showMessageDialog(null,foras[etapa]);
      }else{
        etapa = etapa;
      }
    }

  }

}
