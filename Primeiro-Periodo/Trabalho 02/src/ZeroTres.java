import javax.swing.JOptionPane;


class ZeroTres {

	/**
	 * Faça um programa que solicite ao usuário 10 números inteiros e, ao final, informe a quantidade
	 * de números ímpares e pares lidos. Calcule também a soma dos números pares e a média dos 
	 * números ímpares.
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int numeros, imparcont=0, parcont=0, par =0, impar =0;
		double media;
		
		for (int i = 1; i <= 10; i++){
			numeros = Integer.parseInt(JOptionPane.showInputDialog("Digite um numero"));
			
			if (numeros % 2 == 0){
				parcont++;
				par += numeros;
				
			}else{
				imparcont++;
				impar+= numeros;
				
			}
				
		}
		
		media = impar / imparcont;
		JOptionPane.showMessageDialog(null,"Soma dos Pares =>" + par +" Quantidade de numeros pares =>" + parcont );
		JOptionPane.showMessageDialog(null,"Media dos numeros impares =>" + media + " Quantidade de numeros impares => " + imparcont);
	}

}
