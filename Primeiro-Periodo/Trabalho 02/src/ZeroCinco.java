import javax.swing.JOptionPane;

class ZeroCinco {

	/**
	 * Em uma pesquisa de campo, uma editora solicitou os seguintes dados para os entrevistados:
	 * sexo, idade e quantidade de livros que leu no ano de 2006. Faça um programa que leia os
	 * dados digitados pelo usuário, sendo que deverão ser solicitados dados até que a idade digitada
	 * seja um valor negativo.
	 * Depois, calcule e imprima:
	 * a) A quantidade total de livros lidos pelos entrevistados menores de 10 anos. -ok	 
	 * b) A quantidade de mulheres que leram 5 livros ou mais. -ok
	 * c) A média de idade dos homens que leram menos que 5 livros. ok
	 * d) O percentual de pessoas que não leram livros. -ok
	 */
	public static void main(String[] args) {
		String sexo;
		int idade=0, livros, mulherCin=0, homemCin =0;

		int somaLivro=0, pessoas=0,naoLivro=0, idadeHom=0;

		double totalPor, mediaHomem;
		
		do{
			sexo = JOptionPane.showInputDialog("Insira o Sexo");
			livros = Integer.parseInt(JOptionPane.showInputDialog("Insira a Quantidade de Livros"));
			idade = Integer.parseInt(JOptionPane.showInputDialog("Insira a Idade"));


			if(livros == 0){
				naoLivro++;
			}


			if(idade <= 10){
				somaLivro += livros;
			}


			if(sexo.equals("feminino") && livros >= 5){
				mulherCin++;
			}


			if(sexo.equals("masculino") && livros <=5 ){
				idadeHom+=idade;
				
				homemCin++;
			}



			pessoas++;
		
			


			
		
			
		}while(idade > 0);
			mediaHomem = idadeHom/homemCin;
				totalPor = (naoLivro*100)/pessoas;

				JOptionPane.showMessageDialog(null,"A quantidade total de livros lidos pelos entrevistados menores de 10 anos. "+ somaLivro);
				JOptionPane.showMessageDialog(null,"Quantidade de mulheres que leram mais de 5 livros "+ mulherCin);
				JOptionPane.showMessageDialog(null,"Idade Media dos homens que leram menos de 5 livros "+ mediaHomem);
				JOptionPane.showMessageDialog(null,"Porcentagem de pessoas que não leem livros "+ totalPor);

	}

}
