import javax.swing.JOptionPane;

class ZeroQuatro {
	

	/**
	 * Uma determinada empresa armazena para cada funcionário (10 no total) uma ficha contendo o
	 * código, o número de horas trabalhadas e o seu nº de dependentes.
	 * Considerando que:
	 * 	a. A empresa paga 12 reais por hora e 40 reais por dependentes.
	 * 	b. Sobre o salário são feitos descontos de 8,5% para o INSS e 5% para IR.
	 * Faça um programa para ler o código, número de horas trabalhadas e número de dependentes de
	 * cada funcionário. Após a leitura, escreva qual o código, os valores descontados para cada tipo de 
	 * imposto e finalmente o salário líquido de cada um dos funcionários.
	 */
	public static void main(String[] args) {
		int codigo, dependentes;
		double horas,salario, salarioFin, inss, ir;
		
		final double INSS = 0.085;
		final double IR = 0.05;
		final int DEPENDENTES = 40;
		final int HORAS = 12;
		
		codigo = Integer.parseInt(JOptionPane.showInputDialog("Insira o Codigo do Funcionario"));
		dependentes = Integer.parseInt(JOptionPane.showInputDialog("Insira o Quantidade de Dependentes"));
		horas = Double.parseDouble(JOptionPane.showInputDialog("Insira a Quantidade de Horas"));
		
		salario = (dependentes* DEPENDENTES) + (horas * HORAS);
		inss = (salario * INSS );
		ir = ( salario * IR );
		salarioFin = salario +(inss - ir);
		JOptionPane.showMessageDialog(null, "Codigo do Funcionario -> "+ codigo);
		JOptionPane.showMessageDialog(null, "Desconto de Inss -> " + inss);
		JOptionPane.showMessageDialog(null, "Desconto do IR -> " + ir);
		JOptionPane.showMessageDialog(null, "Salario Liquido" +salarioFin);
		

	}

}
