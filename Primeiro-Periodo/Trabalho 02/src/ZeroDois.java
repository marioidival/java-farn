import javax.swing.JOptionPane;

class ZeroDois {

	/**
	 * Faça um programa que solicite ao usuário para digitar valores numéricos inteiros positivos.
	 * Encerre a entrada de dados quando for digitado um número negativo ou zero. Calcule a média dos 
	 * números positivos digitados.
	 */
	public static void main(String[] args) {
		
		int media, cont=0, soma = 0, numeros;
		
		do{
			
			numeros = Integer.parseInt(JOptionPane.showInputDialog("Digite um Numero: 	"));
			soma = soma + numeros;
			
			cont ++;
			media = soma / cont;
		}while(numeros != 0);
		
		
		JOptionPane.showMessageDialog(null,"Media => "+ media);
		
	}

}
