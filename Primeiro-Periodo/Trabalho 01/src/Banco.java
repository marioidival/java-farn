import javax.swing.JOptionPane;
import java.util.Scanner;

class Banco {
	/*
	 * Um Banco concederá um crédito especial aos seus clientes, variável com o saldo médio no
último ano. Faça um algoritmo que leia o saldo médio de um cliente e calcule o valor do
crédito de acordo com a tabela abaixo. Mostre uma mensagem informando o saldo médio e
o valor do crédito.
	 */
	public static void main(String[] args) {
		
		double salario, credito;
		salario = Double.parseDouble(JOptionPane.showInputDialog("Digite o Salario medio"));
		
		if( salario <= 200.00){
			JOptionPane.showMessageDialog(null,"Nem credito adicional");
			
		}else if( salario >= 201.00 && salario <= 400.00 ){
			credito = salario * 0.80;
			JOptionPane.showMessageDialog(null,"Seu credito é de: "+ credito);
			
		}else if( salario >= 401.00 && salario <= 600.00){
			credito = salario * 0.70;
			JOptionPane.showMessageDialog(null,"Seu credito é de: "+ credito);
			
		}else if( salario > 600){
			credito = salario * 0.60;
			JOptionPane.showMessageDialog(null,"Seu credito é de: "+ credito);
			
		}

	}

}
