import javax.swing.JOptionPane;
class Notas {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		double n1,n2, media;
		int falta, aulas, frequencia;
		
		n1 = Double.parseDouble(JOptionPane.showInputDialog("Insira a primeira nota"));
		n2 = Double.parseDouble(JOptionPane.showInputDialog("Insira a segunda nota"));
		
		media = (n1 + n2 )/2;
		
		aulas = Integer.parseInt(JOptionPane.showInputDialog("Insira a quantidade de aulas ministradas"));
		falta = Integer.parseInt(JOptionPane.showInputDialog("Insira a quantidade de aulas presentes"));
		
		frequencia = (falta * 100)/aulas;
		
		if(media >= 6 && frequencia > 75){
			JOptionPane.showMessageDialog(null,"Aluno Aprovado");
		}else{
			JOptionPane.showMessageDialog(null,"Aluno Reprovado");

		}
		
	}

}
