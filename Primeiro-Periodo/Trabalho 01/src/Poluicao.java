import javax.swing.JOptionPane;
import java.util.Scanner;

class Poluicao {

	private static double taxa;
	/*
	 *  Secretaria de Meio Ambiente, que controla o índice de poluição, mantém 3 grupos de
indústrias que são altamente poluentes do meio ambiente. O índice de poluição aceitável
varia de 0,05 até 0,29. Se o índice sobe para 0,3 as indústrias do 1º grupo são intimadas a
suspenderem suas atividades, se o índice crescer para 0,4 as industrias do 1º e 2º grupo são
intimadas a suspenderem suas atividades, se o índice atingir 0,5 todos os grupos devem ser
notificados a paralisarem suas atividades. Faça um programa que leia o índice de poluição
	 */

	public static void main(String[] args) {
		taxa = Double.parseDouble(JOptionPane.showInputDialog("Digite a taxa de poluicao"));
		
		if(taxa >= 0.05 && taxa <= 0.29){
			JOptionPane.showMessageDialog(null,"Taxa aceitavel");
		}else if (taxa > 0.29 && taxa <= 0.3){
			JOptionPane.showMessageDialog(null,"As indústrias do 1º grupo vão ser intimadas a suspenderem suas atividades");
		}else if (taxa >= 0.4 && taxa <=0.5){
			JOptionPane.showMessageDialog(null," as industrias do 1º e 2º grupo vão ser intimadas a suspenderem suas atividades");
		}else if (taxa > 0.5){
			JOptionPane.showMessageDialog(null," todos os grupos devem sernotificados a paralisarem suas atividades" );
		}
		
	}

}
