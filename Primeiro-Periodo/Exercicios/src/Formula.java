import java.util.Scanner;
class Formula {

	/**
	 * Faça um programa que leia A, B e C e calcule a seguinte fórmula: (7*C+5*B) /
	 * (2*A*3), armazenando-a na variável Result.
	 */
	public static void main(String[] args) {
		float a,b,c;
		double result;
		Scanner dados = new Scanner(System.in);
		
		System.out.println("Digite o valor de A: ");
		a = dados.nextFloat();
		System.out.println("Digite o valor de B: ");
		b = dados.nextFloat();
		System.out.println("Digite o valor de C: ");
		c = dados.nextFloat();
		
		result = (7*c+5*b)/(2*a*3);
		
		System.out.println("O Resultado da formula foi: " + result);
		
		

	}

}
