import java.util.Scanner;
class Troca {

	/**
	 * Faça um programa que leia dois valores nas variáveis A e B respectivamente,
	 * troque o valor contido na variável A pelo valor em B, e o valor em B pelo valor em A,
	 * isto é, imprimiremos A e B com os valores trocados.
	 */
	public static void main(String[] args) {
		Scanner dados = new Scanner(System.in);
		String a,b, aux;
		System.out.println("Digite um valor: ");
		a = dados.next();
		System.out.println("Digite outro valor: ");
		b = dados.next();
		
		aux = a;
		a = b;
		b = aux;
		
		System.out.println("Os valores forão trocados :) -> a = "+a+"? \n e b = "+b+" ?");
		
		

	}

}
