import java.util.Scanner;

class Potencia {

	/**
	 * Faça um programa que leia dois valores inteiros (X e Y) e calcule X na potência Y.
	 * Observação: utilizar a função matemática.
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner dados = new Scanner(System.in);
		System.out.println("Informe um numero para calcular a potencia: ");
		float x = dados.nextFloat();
		
		System.out.println("Informe a potencia para calcular com o numero anterior : ");
		float y = dados.nextFloat();
		double resultado;
		
		resultado = Math.pow(x, y);
		
		System.out.println("O resultado de " +x+" elevado a "+ y+" é de : " + resultado);

		
		

	}

}
