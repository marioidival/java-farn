import java.util.Scanner;
class SomaMedia {

	/**
	 * Faça um programa que leia cinco números inteiros, calcule e escreva a soma, o
	 * produto e a média dos números lidos
	 */
	public static void main(String[] args) {
		int a,b,c,d,e,soma,produto;
		float media;
		Scanner dados = new Scanner(System.in);
		System.out.println("Digite o valor de a:");
		a = dados.nextInt();
		System.out.println("Digite o valor de b:");
		b = dados.nextInt();
		System.out.println("Digite o valor de c:");
		c = dados.nextInt();
		System.out.println("Digite o valor de d:");
		d = dados.nextInt();
		System.out.println("Digite o valor de e:");
		e = dados.nextInt();
		
		soma = a+b+c+d+e;
		produto = a*b*c*d*e;
		media = (a+b+c+d+e)/5;
		
		System.out.println("\n-A soma -> " + soma +"\n- O produto -> "+produto+"\n- a media -> "+media);
		

	}

}
