import java.util.Scanner;
class Dias {
	

	/**
	 * 1) Faça um programa que leia a idade de uma pessoa expressa em anos, meses e
	 * dias e mostre-a expressa apenas em dias.
	 */
	public static void main(String[] args) {
		System.out.println("Informe sua idade:");
		
		Scanner dados = new Scanner(System.in);
		int anos = dados.nextInt();
		
		System.out.println("Informe os meses ...");
		int mes = dados.nextInt();
		
		System.out.println("Informe os dias ... :");
		int dia = dados.nextInt();
		
		int anosDias, mesDias;
		anosDias = anos*365;
		mesDias = mes*30;
		
		System.out.println("Sua idade total em dias é : " + anosDias + mesDias + dia);

	}

}
