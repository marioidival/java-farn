import javax.swing.JOptionPane;

class Ibge {

	public static void main(String[] args) {

		int idade, sair=1,contH=0, contF=0, finalM=0, finalF=0,idadeM=0,idadeF=0, NE, N, CO, S, SE;
		int contBranco=0, contNegro=0, contPardo=0, contIndio=0, idadeNegro=0;
		int brancoNE=0, brancoN =0, brancoCO=0, brancoS=0, brancoSE=0;
		int negroNE=0, negroN=0, negroCO=0, negroS=0, negroSE=0;
		int pardoNE=0, pardoN=0, pardoCO=0, pardoS=0, pardoSE=0;
		int indioNE=0, indioN=0, indioCO=0, indioS=0, indioSE=0;
		int mascNE=0, femNE=0, total,totalNE=0, negao=0, masc=0, femi=0;
		double somaIdade=0, porcentNE, mediaNegro, mediaBrasil, idadeMasNE =0, idadeFemNE=0;
		String sexo, regiao, etnia;
		

		for(int i = 1; i > 0; i++){

			idade = Integer.parseInt(JOptionPane.showInputDialog("Qual sua idade?"));
			sexo = JOptionPane.showInputDialog("Qual o seu sexo? \n-m para masculino\n-f para feminino");
			etnia = JOptionPane.showInputDialog("Qual sua Etnia?");
			regiao = JOptionPane.showInputDialog("Qual sua Região ?");
			sair = Integer.parseInt(JOptionPane.showInputDialog(" 0 para Sair"));

			somaIdade += idade;
			
			if(sexo.equals("m")){
				idadeM += idade;
				masc++;
				if(regiao.equals("nordeste")){
					idadeMasNE+=idadeM;
					mascNE++;
				}

				if(etnia.equals("negro")){
					idadeNegro += idade;
					negao++;
				}

				contH++;
			}else if(sexo.equals("f")){
				idadeF += idade;
				femi++;
				if(regiao.equals("nordeste")){
					idadeFemNE+=idadeF;
					femNE++;
				}
				

				if(etnia.equals("negro"))
					idadeNegro += idade;
					negao++;
				
				contF++;
			}else{
				JOptionPane.showMessageDialog(null,"Insira Sexo Valido");
			}

			if(etnia.equals("branco") && regiao.equals("nordeste")){
				brancoNE++;
			}else if(etnia.equals("branco") && regiao.equals("norte")){
				brancoN++;
			}else if(etnia.equals("branco") && regiao.equals("sul")){
				brancoS++;
			}else if(etnia.equals("branco") && regiao.equals("sudeste")){
				brancoSE++;
			}else if(etnia.equals("branco") && regiao.equals("centro-oeste")){
				brancoCO++;
			}else if(etnia.equals("negro") && regiao.equals("norte")){
				negroN++;
			}else if(etnia.equals("negro") && regiao.equals("sul")){
				negroS++;
			}else if(etnia.equals("negro") && regiao.equals("sudeste")){
				negroSE++;
			}else if(etnia.equals("negro") && regiao.equals("centro-oeste")){
				negroCO++;
			}else if(etnia.equals("negro") && regiao.equals("nordeste")){
				negroNE++;
			}else if(etnia.equals("pardo") && regiao.equals("norte")){
				pardoN++;
			}else if(etnia.equals("pardo") && regiao.equals("sul")){
				pardoS++;
			}else if(etnia.equals("pardo") && regiao.equals("sudeste")){
				pardoSE++;
			}else if(etnia.equals("pardo") && regiao.equals("centro-oeste")){
				pardoCO++;
			}else if(etnia.equals("pardo") && regiao.equals("nordeste")){
				pardoNE++;
			}else if(etnia.equals("indio") && regiao.equals("norte")){
				indioN++;
			}else if(etnia.equals("indio") && regiao.equals("sul")){
				indioS++;
			}else if(etnia.equals("indio") && regiao.equals("sudeste")){
				indioSE++;
			}else if(etnia.equals("indio") && regiao.equals("centro-oeste")){
				indioCO++;
			}else if(etnia.equals("indio") && regiao.equals("nordeste")){
				indioNE++;
			}else{
				JOptionPane.showMessageDialog(null,"Insira Etnia e Região Correta");
			}

			if(sair == 0){

				mediaBrasil = somaIdade/(contH+contF);
				JOptionPane.showMessageDialog(null,"Idade Media dos Brasileiros ->\n" + mediaBrasil);
				JOptionPane.showMessageDialog(null,"Idade Media dos Homens\n"+ (double)(idadeM/contH));
				JOptionPane.showMessageDialog(null,"Idade Media das Mulheres\n"+ (double)(idadeF/contF));


				JOptionPane.showMessageDialog(null,"Porcentagem dos Homens e Mulheres no Nordeste:\nHomens ->"+ (double)((mascNE*100)/masc)+" %\nMulheres -> "+ (double)((femNE*100)/ femi));
				mediaNegro = idadeNegro/negao;
				JOptionPane.showMessageDialog(null,"Idade Media dos Negros \n" + mediaNegro);


				if(brancoN > negroN){
					if(brancoN > pardoN){
						if(brancoN > indioN){
							JOptionPane.showMessageDialog(null,"BRANCO NORTE");

						}else if(indioN > negroN){
							if(indioN > pardoN){
								JOptionPane.showMessageDialog(null,"indio norte");
							}
						}
					}else if(pardoN > negroN){
						if(pardoN > indioN){
							JOptionPane.showMessageDialog(null,"Pardo NORTE");
						}
					} 
				}else if(negroN > pardoN)
					if(negroN > indioN)
						JOptionPane.showMessageDialog(null,"NEGRO NORTE");


				if(brancoS > negroS){
					if(brancoS > pardoS){
						if(brancoS > indioS){
							JOptionPane.showMessageDialog(null,"BRANCO SUL");

						}else if(indioS > negroS){
							if(indioS > pardoS){
								JOptionPane.showMessageDialog(null,"indio SUL");
							}
						}
					}else if(pardoS > negroS){
						if(pardoS > indioS){
							JOptionPane.showMessageDialog(null,"Pardo SUL");
						}
					} 
				}else if(negroS> pardoS)
					if(negroS> indioS)
						JOptionPane.showMessageDialog(null,"NEGRO SUL");


				if(brancoNE > negroNE){
					if(brancoNE > pardoNE){
						if(brancoNE > indioNE){
							JOptionPane.showMessageDialog(null,"BRANCO NORDESTE");

						}else if(indioNE > negroNE){
							if(indioNE > pardoNE){
								JOptionPane.showMessageDialog(null,"indio NORDESTE");
							}
						}
					}else if(pardoNE > negroNE){
						if(pardoNE > indioNE){
							JOptionPane.showMessageDialog(null,"Pardo NORDESTE");
						}
					} 
				}else if(negroNE> pardoNE)
					if(negroNE> indioNE)
						JOptionPane.showMessageDialog(null,"NEGRO NORDESTE");




				
				if(brancoSE > negroSE){
					if(brancoSE > pardoSE){
						if(brancoSE > indioSE){
							JOptionPane.showMessageDialog(null,"BRANCO SUDESTE");

						}else if(indioSE > negroSE){
							if(indioSE > pardoSE){
								JOptionPane.showMessageDialog(null,"indio SUDESTE");
							}
						}
					}else if(pardoSE > negroSE){
						if(pardoSE > indioSE){
							JOptionPane.showMessageDialog(null,"Pardo SUDESTE");
						}
					} 
				}else if(negroSE > pardoSE)
					if(negroSE > indioSE)
						JOptionPane.showMessageDialog(null,"NEGRO SUDESTE");





					
				if(brancoCO > negroCO){
					if(brancoCO > pardoCO){
						if(brancoCO > indioCO){
							JOptionPane.showMessageDialog(null,"BRANCO CENTRO-OESTE");

						}else if(indioCO > negroCO){
							if(indioCO > pardoCO){
								JOptionPane.showMessageDialog(null,"indio CENTRO-OESTE");
							}
						}
					}else if(pardoCO > negroCO){
						if(pardoCO > indioCO){
							JOptionPane.showMessageDialog(null,"Pardo CENTRO-OESTE");
						}
					} 
				}else if(negroCO > pardoCO)
					if(negroCO > indioCO)
						JOptionPane.showMessageDialog(null,"NEGRO CENTRO-OESTE");




				break;
			}
		}

	}
}
